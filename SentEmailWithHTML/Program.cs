﻿using System.Net.Mail;
using System.Net;

string emailForSend = "netjprogram@gmail.com"; // tạo ra 1 email giả để mà làm
string appPasswordConfiguration = "nruuhifutemjxgac"; // tạo 2FA rồi sau đó bật cái app password lên

SmtpClient smtpClient = new SmtpClient
{
    Port = 587,
    EnableSsl = true,
    Host = "smtp.gmail.com",
    UseDefaultCredentials = false,
    Credentials = new NetworkCredential(emailForSend, appPasswordConfiguration),
};

string content = await File.ReadAllTextAsync("template.html");

MailMessage message = new MailMessage
{
    Subject = "Hello J Program",
    Body = content,
    From = new MailAddress(emailForSend),
    IsBodyHtml = true
};

message.To.Add(new MailAddress("def@gmail.com")); // muốn gửi cho ai thì điền vô đây
message.To.Add(new MailAddress("abc@gmail.com")); // thêm vài người nữa

await smtpClient.SendMailAsync(message);