Dưới đây là hướng dẫn sử dụng "mật khẩu ứng dụng" để đăng nhập vào Tài khoản Google của bạn:

Bước 1: Đầu tiên, bạn cần truy cập Tài khoản Google của mình.

Bước 2: Sau đó, chọn tab "Bảo mật" để đi đến cài đặt bảo mật. (https://myaccount.google.com/security)

Bước 3: Dưới phần "Đăng nhập vào Google," bạn cần bật tính năng "Xác minh 2 bước."(https://myaccount.google.com/signinoptions/two-step-verification). Điều này là quan trọng để có thể sử dụng "mật khẩu ứng dụng." (https://support.google.com/accounts/answer/185839)

Bước 4: Tiếp theo, bạn sẽ thấy tùy chọn "Mật khẩu ứng dụng"(App password) xuất hiện. Chọn nó để tiếp tục.

Bước 5: Trước tiên, bạn sẽ được yêu cầu nhập một tên để giúp bạn nhớ mục đích sử dụng của "mật khẩu ứng dụng."

Bước 6: Sau đó, chọn "Tạo" để tạo một "mật khẩu ứng dụng" mới.

Bước 7: Một mã "mật khẩu ứng dụng" sẽ được tạo và hiển thị trên màn hình. Đây là mã 16 ký tự duy nhất và bạn cần nó để đăng nhập vào ứng dụng hoặc thiết bị.

Bước 8: Cuối cùng, sau khi bạn đã sao chép mã "mật khẩu ứng dụng," chọn "Hoàn tất" để hoàn thành quy trình.

Lưu ý: Nếu bạn không thể tìm thấy tùy chọn "Mật khẩu ứng dụng," điều này có thể do một số lý do sau đây:

-   Tài khoản Google của bạn chỉ có Xác minh 2 bước được cài đặt cho các chìa khóa bảo mật.
-   Bạn đã đăng nhập vào tài khoản của công ty, trường học hoặc tài khoản tổ chức khác.
-   Tài khoản Google của bạn đã được bảo vệ nâng cao.

Lưu ý cuối cùng: Thông thường, bạn sẽ chỉ cần nhập mã "mật khẩu ứng dụng" một lần cho mỗi ứng dụng hoặc thiết bị mà bạn muốn kết nối với Tài khoản Google của mình.

Tài liệu hướng dẫn chi tiết: https://support.google.com/accounts/answer/185833
videos để làm theo https://www.youtube.com/watch?v=dHZZtG3fcXc